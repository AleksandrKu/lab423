<?php

namespace  application\libraries;
class PDODriver
{

	private $connection;
	public function __construct($server, $username, $password, $database)
	{
		$this->connection = new \PDO("mysql:host={$server};dbname={$database}", $username, $password);
	}

	public function getConnection()
	{
		return $this->connection;
	}

	public function __destruct()
	{
		//сам закрывает конекшены
	}




}